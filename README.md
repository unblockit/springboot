# UnblockIT

## What is monitoring?
O monitoramento é uma forma de se manter atualizado sobre a integridade e o desempenho de suas APIs. O serviço de monitoramento integrado do `springboot` ajuda a consolidar uma etapa adicional em seu ciclo de vida de desenvolvimento de API.

## Getting Started

Os endpoints do `Spring Boot Actuator` nos permitem monitorar e interagir com nosso aplicativo. Inclui vários terminais integrados e permite-nos adicionar nossos próprios indicadores.

## Guides
Os guias a seguir ilustram como usar alguns recursos concretamente:

* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/reference/htmlsingle/#production-ready)
* [Health Information](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/reference/htmlsingle/#production-ready-health)
* [Auto-configured HealthIndicators](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/reference/htmlsingle/#production-ready-health-indicators)
* [WritingCustom Health Indicators](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/reference/htmlsingle/#writing-custom-healthindicators)
* [Prometheus](https://docs.spring.io/spring-boot/docs/2.3.3.RELEASE/reference/html/production-ready-features.html#production-ready-metrics-export-prometheus)
* [Registering Custom Metrics](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html#production-ready-metrics-custom)