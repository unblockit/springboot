package com.everis.unblockit.controller.v1

import com.everis.unblockit.controller.WeatherForecastController
import com.everis.unblockit.model.WeatherForecast
import com.everis.unblockit.service.WeatherForecastService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class WeatherForecastControllerImpl @Autowired constructor(private val weatherForecastService: WeatherForecastService) :
        WeatherForecastController {

    override fun get(id: String): Optional<WeatherForecast> {
        return weatherForecastService.get(id)
    }

    override fun all(pageable: Pageable): Page<WeatherForecast> {
        return weatherForecastService.getAll(pageable)
    }
}