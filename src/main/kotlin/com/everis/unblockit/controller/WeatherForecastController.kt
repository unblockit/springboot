package com.everis.unblockit.controller

import com.everis.unblockit.model.WeatherForecast
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import java.util.*

@RequestMapping("/wf")
interface WeatherForecastController {

    @GetMapping
    fun all(pageable: Pageable): Page<WeatherForecast>

    @GetMapping("{id}")
    fun get(@PathVariable("id") id: String): Optional<WeatherForecast>
}