package com.everis.unblockit.metrics

import com.mongodb.BasicDBObject
import io.micrometer.core.instrument.Gauge
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.binder.MeterBinder
import org.springframework.data.mongodb.core.MongoTemplate

class MongoStatusProbe constructor(private val template: MongoTemplate) : MeterBinder {

    private val up = 1.0
    private val down = 0.0

    override fun bindTo(registry: MeterRegistry) {
        Gauge.builder("datasource_connection", this, { value -> if (value.status()) up else down })
                .description("Datasource connection status")
                .tags("datasource", "mongo")
                .baseUnit("status")
                .register(registry)
    }

    private fun status(): Boolean {
        return try {
            template.db
                    .runCommand(BasicDBObject("ping", "1"))
                    .getDouble("ok").equals(1.0)
        } catch (e: Exception) {
            false
        }
    }
}