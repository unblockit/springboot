package com.everis.unblockit.health

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component

@Component
class MainframeHealthIndicator @Autowired constructor(private val template: MongoTemplate) : HealthIndicator {
    override fun health(): Health {
        return try {
            Health.up().withDetail("version", this.template
                    .executeCommand("{ buildInfo: 1 }")
                    .getString("version"))
                    .build()
        } catch (e: Exception) {
            return Health.down().withException(e).build()
        }
    }
}